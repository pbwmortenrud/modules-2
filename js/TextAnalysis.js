export class TextAnalysis {
  constructor(text) {
    this.text = text; //String
    this.wordList = this.getWordList(); //Map
    this.letterList = this.getLetterList(); //Map
    this.lettersTotal = this.getLettersTotal(); //Number
  }

  getWordList() {
    let words = this.wordListMap();
    return words;
  }

  getLetterList() {
    let letters = this.letterFrequencyMap();
    return letters;
  }
  getLettersTotal() {
      let count = this.lettersTotal();
      return count;
  }

  wordListMap() {
    let wordarr = this.wordListArray();
    let wordList = new Map();

    for (let word of wordarr) {
      word = word.toLowerCase();
      let c = 1;
      if (wordList.get(word)) {
        c = wordList.get(word);
        c++;
      }
      wordList.set(word, c);
    }

    wordList = this.sortMap(wordList);
    return wordList;
  }

  wordListArray() {
    let wordArr = [];
    const regex = /[a-zA-ZæøåÆØÅ]+/gi;
    let match = regex.exec(this.text);

    while (match) {
      let word = match[0] + ""; //force string;
      wordArr.push(word.toLowerCase());
      match = regex.exec(this.text);
    }
    return wordArr;
  }

  letterFrequencyMap() {
    let letterMap = new Map();
    const regex = /[a-zæøåÆØÅ]/gi;
    let match = regex.exec(this.text);
    while (match) {
      let letter = match[0].toLocaleLowerCase();
      let c = 1;
      if (letterMap.get(letter)) {
        c = letterMap.get(letter);
        c++;
      }
      letterMap.set(letter, c);
      match = regex.exec(this.text);
    }
    letterMap = this.sortMap(letterMap);
    return letterMap;
  }

  sortMap(mapToSort) {
    return new Map([...mapToSort.entries()].sort());
  }

  wordListToString() {
    let s = `s`;
    //TODO: print unique words
    for (const [key, value] of this.wordList) {
      s += `${key}: ${value} \n`;
    }
    return s;
  }

  letterFrequencyToString() {
    //TODO: print letters with count and percentage
    let s = ``;
    for (const [key, value] of this.letterList) {
        let percentage = (100 / this.lettersTotal * value).toFixed(2);
        s += `${key}: ${value}, ${percentage}% \n`;
      }
    return s;
  }

  letterFrequencyToChart() {
    //TODO: print letters in a chart format, with signs representing 0.5% each
    let s = ``;
    for (const [key, value] of this.letterList) {
        let percentage = (100 / this.lettersTotal * value).toFixed(2);
        let stars = ``;
        for (let i = 0; i < Math.round(percentage*2); i++) {
            stars += `*`;
        }
        s += `${key}: ${stars} \n`;
      }
    return s;
  }

  lettersTotal() {
    let count = 0;
    const regex = /[a-zæøåÆØÅ]/gi;
    let match = regex.exec(this.text);
    while (match) {
      count++;
      match = regex.exec(this.text);
    }
    return count;
  }
}
