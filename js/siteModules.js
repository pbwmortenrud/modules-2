import { menu } from "./menu.js";
import { $, $ce } from "./domLib.js";

export function makeMenu(where) {
  let nav = $ce("nav");
  let list = $ce("ul");
  nav.appendChild(list);
  menu.forEach(function (item) {
    let listitem = $ce("li");
    let link = $ce("a");
    link.setAttribute("href", item.url);
    link.setAttribute("title", item.title);
    link.classList.add("menu-link");
    let txt = document.createTextNode(item.text);
    link.appendChild(txt);
    listitem.appendChild(link);
    list.appendChild(listitem);
  });
  $(where).appendChild(nav);
}
