import { makeMenu } from "./siteModules.js";
import { lyseNatter, sample1 } from "./TextAnalysisHandler.js";
import { TextAnalysis } from "./TextAnalysis.js";

const initPage = function () {
  makeMenu('menu');

}
window.addEventListener('load', initPage);

let analysis = new TextAnalysis(lyseNatter);

// console.log(analysis.wordListArray());
// console.log(analysis.wordListMap());
// console.log(analysis.letterFrequencyMap());
// console.log(analysis.wordList);
// console.log(analysis.letterList);
console.log(analysis.wordListToString());
console.log(analysis.lettersTotal);
console.log(analysis.letterFrequencyToString());
console.log(analysis.letterFrequencyToChart());